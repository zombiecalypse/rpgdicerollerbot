import unittest
import dice
import random
import math
import time
import functools
import pyparsing

from hypothesis import given, settings, note, example, assume
import hypothesis.strategies as st
import hypothesis.stateful as stat
import hypothesis

a_die = st.builds(dice.Dice,
        st.integers(min_value=1, max_value=10000), st.integers(min_value=1))
a_bonus = st.integers()


with settings(max_examples=500, timeout=hypothesis.unlimited, deadline=15e3):
    def IgnoreOutOfBounds(f):
        @functools.wraps(f)
        def _f(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except dice.OutOfBoundsError:
                return
        return _f

    class TestStatefulMachine(stat.RuleBasedStateMachine):
        def __init__(self):
            super(TestStatefulMachine, self).__init__()
            self.last = None
            self.other = None
            self.rand = random.Random(0)

        expressions = stat.Bundle('expressions')

        @stat.rule(target=expressions, e1=a_die)
        def Die(self, e1):
            self.other = self.last
            self.last = e1
            return e1

        @stat.rule(target=expressions, bonus=a_bonus)
        def Die(self, bonus):
            self.other = self.last
            self.last = bonus
            return bonus

        @stat.rule(target=expressions, es=st.lists(expressions, min_size=1))
        def Max(self, es):
            self.other = self.last
            self.last = dice.Max(reduce(dice.Comma, es))
            return self.last

        @stat.rule(target=expressions, es=st.lists(expressions, min_size=1))
        def Min(self, es):
            self.other = self.last
            self.last = dice.Min(reduce(dice.Comma, es))
            return self.last

        @stat.rule(target=expressions, es=st.lists(expressions, min_size=1))
        def Sum(self, es):
            self.other = self.last
            self.last = dice.Sum(reduce(dice.Comma, es))
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Comma(self, e1, e2):
            self.other = self.last
            self.last = dice.Comma(e1, e2)
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Add(self, e1, e2):
            self.other = self.last
            self.last = dice.Add(e1, e2)
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Sub(self, e1, e2):
            self.other = self.last
            self.last = dice.Sub(e1, e2)
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Mul(self, e1, e2):
            self.other = self.last
            self.last = dice.Mul(e1, e2)
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Div(self, e1, e2):
            self.other = self.last
            self.last = dice.Div(e1, e2)
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Over(self, e1, e2):
            self.other = self.last
            self.last = dice.Over(e1, e2)
            return self.last

        @stat.rule(target=expressions, e1=expressions, e2=expressions)
        def Under(self, e1, e2):
            self.other = self.last
            self.last = dice.Under(e1, e2)
            return self.last

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def CalcReturn(self):
            e = self.last
            r = dice.Calc(e, time.time() + 1)
            assert isinstance(r, dice.Results)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def CalcEq(self):
            r = dice.Calc(self.last, time.time() + 1)
            l = dice.Calc(self.last, time.time() + 1)
            assume(not isinstance(r.results.keys()[0], float))
            assert l == r, repr((l, r))

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def CalcTypes(self):
            r = dice.Calc(self.last, time.time() + 10.)
            assert isinstance(r.results, dict)
            assert isinstance(r.results.values()[0], float), str(r.results.values()[0])
            assert all(type(r.results.values()[0]) == type(t)
                    for t in r.results.values()), str(r.results.values())
            assert all(type(r.results.keys()[0]) == type(t)
                    for t in r.results.keys()), str(r.results.keys())
            assert isinstance(r.results.keys()[0], (int, long, tuple)), str(r.results.keys()[0])
            if isinstance(r.results.keys()[0], tuple):
                assert isinstance(r.results.keys()[0][0], (int, long)), str(r.results.keys()[0][0])

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def CalcTypesOfDisplay(self):
            r = dice.Calc(self.last, time.time() + 1)
            s = dice.DisplayCalc(r)
            assert isinstance(s, str), repr(s)
            assert len(s) > 0, repr(s)
            assert len(s) < 1000, repr(s) 

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def EvalReturn(self):
            r, rolls = dice.Eval(self.last, self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long, list, tuple)), repr(r)
            assert all(isinstance(s, str) for s in rolls), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def EvalReturnSum(self):
            r, rolls = dice.Eval(dice.Sum(self.last), self.rand)
            if isinstance(r, float):
                assert math.isnan(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def EvalReturnMax(self):
            r, rolls = dice.Eval(dice.Max(self.last), self.rand)
            if isinstance(r, float):
                assert math.isnan(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None)
        @IgnoreOutOfBounds
        def EvalReturnMin(self):
            r, rolls = dice.Eval(dice.Min(self.last), self.rand)
            if isinstance(r, float):
                assert math.isnan(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.rule(o=expressions)
        def MakeOther(self, o):
            self.other = o

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None and self.other is not None)
        @IgnoreOutOfBounds
        def EvalAdd(self):
            r, rolls = dice.Eval(dice.Add(self.last, self.other), self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None and self.other is not None)
        @IgnoreOutOfBounds
        def EvalSub(self):
            r, rolls = dice.Eval(dice.Sub(self.last, self.other), self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None and self.other is not None)
        @IgnoreOutOfBounds
        def EvalMul(self):
            r, rolls = dice.Eval(dice.Mul(self.last, self.other), self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None and self.other is not None)
        @IgnoreOutOfBounds
        def EvalDiv(self):
            r, rolls = dice.Eval(dice.Div(self.last, self.other), self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long)), repr(r)

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None and self.other is not None)
        @IgnoreOutOfBounds
        def EvalOver(self):
            r, rolls = dice.Eval(dice.Over(self.last, self.other), self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long)), repr(r)
                assert r >= 0

        @stat.invariant()
        @stat.precondition(lambda self: self.last is not None and self.other is not None)
        @IgnoreOutOfBounds
        def EvalUnder(self):
            r, rolls = dice.Eval(dice.Under(self.last, self.other), self.rand)
            if isinstance(r, float):
                assert math.isnan(r), repr(r)
            else:
                assert isinstance(r, (int, long)), repr(r)
                assert r >= 0
    TestStateful = TestStatefulMachine.TestCase

    class TestEval(unittest.TestCase):
        def setUp(self):
            self.rand = random.Random(0)

        @given(a_die)
        def test_eval_return_die(self, d):
            try:
                r, rolls = dice.Eval(d, self.rand)
            except dice.OutOfBoundsError:
                return
            assert isinstance(r, (list,)), repr(r)

        @given(a_die, st.integers())
        def test_eval_over(self, d, i):
            try:
                r, rolls = dice.Eval(dice.Over(d, i), self.rand)
            except dice.OutOfBoundsError:
                return
            assert isinstance(r, (int, long)), repr(r)
            assert 0 <= r <= d.n, repr(r)

        @given(a_die, st.integers())
        def test_eval_under(self, d, i):
            try:
                r, rolls = dice.Eval(dice.Under(d, i), self.rand)
            except dice.OutOfBoundsError:
                return
            assert isinstance(r, (int, long)), repr(r)
            assert 0 <= r <= d.n, repr(r)

        @given(a_die, a_die)
        def test_eval_comma(self, d1, d2):
            try:
                r, rolls = dice.Eval(dice.Comma(d1, d2), self.rand)
            except dice.OutOfBoundsError:
                return
            assert isinstance(r, tuple), repr(r)
            assert len(r) >= 2, repr(r)
            assert len(r) <= d1.n + d2.n, repr(r)
            for a in r:
                assert 0 < a <= max(d1.d, d2.d), repr(r)

    class TestParse(unittest.TestCase):
        @given(a_die)
        def test_show_die(self, d):
            r = dice.Show(d)
            assert isinstance(r, (str,))
            re = dice.Parse(r).e
            self.assertEqual(re, d)

        def test_parse_long_sum(self):
            s = ['d20', 'd4', '5']
            eval = (lambda x:
                    dice.Eval(dice.Parse(x.format(*s)),
                            random.Random(0)))
            self.assertEqual(
                    eval('{} + {} + {}'),
                    eval('{} + ({} + {})'))
            self.assertEqual(
                    eval('({} + {}) + {}'),
                    eval('{} + ({} + {})'))

        def test_parse_long_comma(self):
            s = ['d20', 'd4', '5']
            eval = (lambda x:
                    dice.Eval(dice.Parse(x.format(*s)),
                            random.Random(0)))
            self.assertEqual(
                    eval('{} , {} , {}'),
                    eval('{} , ({} , {})'))
            self.assertEqual(
                    eval('({} , {}) , {}'),
                    eval('{} , ({} , {})'))

        def test_parse_long_prod(self):
            s = ['d20', 'd4', '5']
            eval = (lambda x:
                    dice.Eval(dice.Parse(x.format(*s)),
                            random.Random(0)))
            self.assertEqual(
                    eval('{} * {} * {}'),
                    eval('{} * ({} * {})'))
            self.assertEqual(
                    eval('({} * {}) * {}'),
                    eval('{} * ({} * {})'))

        def test_parse_bad(self):
            wrong = [
                    'd20 +',
                    'x',
            ]
            right = [
                    ''
            ]
            for x in wrong:
                with self.assertRaises(pyparsing.ParseException):
                    dice.Parse(x)
            for x in right:
                dice.Parse(x)
