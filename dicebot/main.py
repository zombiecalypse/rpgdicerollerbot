import StringIO
import json
import logging
import random
import urllib
import urllib2
import httplib
import re
import dice
import os
import os.path
import tok  # provide your own
import time
import pyparsing

# standard app engine imports
from google.appengine.api import urlfetch
from google.appengine.ext import ndb
import webapp2
from google.appengine.api import urlfetch

BASE_URL = 'https://api.telegram.org/bot' + tok.TOKEN + '/'

urlfetch.set_default_fetch_deadline(60)



# ================================

class EnableStatus(ndb.Model):
    # key name: str(chat_id)
    enabled = ndb.BooleanProperty(indexed=False, default=False)


# ================================

def setEnabled(chat_id, yes):
    es = EnableStatus.get_or_insert(str(chat_id))
    es.enabled = yes
    es.put()

def getEnabled(chat_id):
    es = EnableStatus.get_by_id(str(chat_id))
    if es:
        return es.enabled
    return False


# ================================

class MeHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        fetched = urlfetch.fetch(BASE_URL + 'getMe')
        self.response.write(json.dumps(json.loads(fetched.content)))


class GetUpdatesHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'getUpdates'))))


class SetWebhookHandler(webapp2.RequestHandler):
    def get(self):
        urlfetch.set_default_fetch_deadline(60)
        url = self.request.get('url')
        if url:
            self.response.write(json.dumps(json.load(urllib2.urlopen(BASE_URL + 'setWebhook', urllib.urlencode({'url': url})))))


class WebhookHandler(webapp2.RequestHandler):
    def post(self):
        urlfetch.set_default_fetch_deadline(60)
        body = json.loads(self.request.body)
        logging.info('request body:')
        logging.info(body)

        update_id = body['update_id']
        try:
            message = body['message']
        except:
            message = body['edited_message']
        message_id = message.get('message_id')
        date = message.get('date')
        text = message.get('text')
        fr = message.get('from')
        chat = message['chat']
        chat_id = chat['id']

        if not text:
            logging.info('no text')
            return

        def reply(msg=None, img=None):
            logging.info('send response:')
            resp = None
            if msg:
                reply = {
                    'chat_id': chat_id,
                    'text': msg.encode('utf-8'),
                    'disable_web_page_preview': True,
                    'reply_to_message_id': message_id,
                }
                headers = {
                        'Content-Type': 'application/x-www-form-urlencoded'
                }
                logging.info('reply:\n%s', reply)
                resp = urlfetch.fetch(
                        url = BASE_URL+'sendMessage',
                        payload=urllib.urlencode(reply),
                        method=urlfetch.POST,
                        headers=headers)
                resp = (resp.status_code, resp.content)
            elif img:
                resp = multipart.post_multipart(BASE_URL + 'sendPhoto', [
                    ('chat_id', str(chat_id)),
                    ('reply_to_message_id', str(message_id)),
                ], [
                    ('photo', 'image.jpg', img),
                ])
            else:
                logging.error('no msg or img specified')
                resp = None

            logging.info(resp)

        if text.startswith('/'):
            text = text.strip()
            try:
                command, rest = re.split('\s+', text, 1)
            except ValueError:
                command = text
                rest = ''
            if command == '/start':
                reply('Bot enabled')
                setEnabled(chat_id, True)
            elif command == '/stop':
                reply('Bot disabled')
                setEnabled(chat_id, False)
            elif command == '/roll':
                seed = random.getrandbits(64)
                r = random.Random(seed)
                try:
                    s = dice.Parse(rest)
                    res, rolls = dice.Eval(s, r)
                    reply('rolled:\n{1}\n\n{0} '.format(res,
                            '\n'.join(rolls)))
                except dice.OutOfBoundsError as e:
                    reply(str(e))
                except pyparsing.ParseException as e:
                    reply('%r' % e)
            elif command == '/calc':
                s = dice.Parse(rest)
                try:
                    res = dice.Calc(s, time.time()+10.)
                    reply('chances:\n\n{0} '.format(dice.DisplayCalc(res)))
                except dice.OutOfBoundsError as e:
                    reply(str(e))



app = webapp2.WSGIApplication([
    ('/me', MeHandler),
    ('/updates', GetUpdatesHandler),
    ('/set_webhook', SetWebhookHandler),
    ('/webhook', WebhookHandler),
], debug=True)
