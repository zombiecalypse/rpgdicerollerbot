import time
import pyparsing
import collections
import operator
import itertools
import functools
import numpy

class OutOfBoundsError(ValueError):
    pass

Dice = collections.namedtuple('Dice', ['n', 'd'])
Max = collections.namedtuple('Max', ['l'])
Min = collections.namedtuple('Min', ['l'])
Sum = collections.namedtuple('Sum', ['l'])
Mul = collections.namedtuple('Mul', ['a', 'b'])
Div = collections.namedtuple('Div', ['a', 'b'])
Add = collections.namedtuple('Add', ['a', 'b'])
Sub = collections.namedtuple('Sub', ['a', 'b'])
Over = collections.namedtuple('Over', ['a', 'b'])
Under = collections.namedtuple('Under', ['a', 'b'])
Comma = collections.namedtuple('Comma', ['a', 'b'])
Named = {
    Add : operator.add,
    Sub : operator.sub,
    Mul : operator.mul,
    Div : operator.div,
    Min : min,
    Max : max,
    Sum : sum,
    Over : lambda x, y: sum(a >= y for a in x),
    Under : lambda x, y: sum(a <= y for a in x),
    Comma : lambda x, y: ((tuple(x) if isinstance(x, (list, tuple)) else (x,)) +
        (tuple(y) if isinstance(y, (list, tuple)) else (y,))),
}

Expr = collections.namedtuple('Expr', ['e'])

d = (pyparsing.Optional(pyparsing.Word(pyparsing.nums), default='1') + pyparsing.Suppress('d') +
        pyparsing.Word(pyparsing.nums)).setParseAction(lambda x: Dice(int(x[0],
                10),
                int(x[1], 10)))
bonus = pyparsing.Word(pyparsing.nums).setParseAction(lambda x: int(x[0], 10))

s = pyparsing.Suppress

expr = pyparsing.Optional(pyparsing.infixNotation(
        baseExpr=(d | bonus),
        opList=[
                (s('sum'), 1, pyparsing.opAssoc.RIGHT, lambda (x,): Sum(x[0])),
                (s('max'), 1, pyparsing.opAssoc.RIGHT, lambda (x,): Max(x[0])),
                (s('min'), 1, pyparsing.opAssoc.RIGHT, lambda (x,): Min(x[0])),
                (s('-'), 1, pyparsing.opAssoc.RIGHT, lambda (x,): Sub(0, x[0])),
                (s('*'), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Mul, x)),
                (s('/'), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Div, x)),
                (s('+'), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Add, x)),
                (s('-'), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Sub, x)),
                (s('over'), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Over, x)),
                (s('under'), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Under, x)),
                (s(','), 2, pyparsing.opAssoc.LEFT, lambda (x,): reduce(Comma, x)),
]).setParseAction(lambda (x,): Expr(x)))
expr.enablePackrat()

def _Normalize(x):
    if x is None:
        return [0]
    if isinstance(x, Expr):
        return _Normalize(x.e)
    if isinstance(x, (Sum, Max, Min)):
        return type(x)(_Normalize(x.l))
    if isinstance(x, (Add, Sub, Mul, Div, Over, Under, Comma)):
        return type(x)(_Normalize(x.a), _Normalize(x.b))
    if isinstance(x, (long, int, Dice)):
        return x
    raise NotImplementedError('type %s not handled: %s' % (type(x), x))


def Parse(s):
    assert isinstance(s, basestring)
    a = list(expr.parseString(s.lower(), parseAll=True))
    if a:
        return Expr(_Normalize(a[0]))
    else:
        return 0

def Show(x):
    if isinstance(x, Expr):
        return '(%s)' % Show(x.e)
    if isinstance(x, Dice):
        return '%id%i' % (x.n, x.d)
    if isinstance(x, Sum):
        return 'sum (%s)' % Show(x.l)
    if isinstance(x, Max):
        return 'max (%s)' % Show(x.l)
    if isinstance(x, Min):
        return 'min (%s)' % Show(x.l)
    if isinstance(x, Add):
        return '(%s) + (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, Sub):
        return '(%s) - (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, Mul):
        return '(%s) * (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, Div):
        return '(%s) / (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, Under):
        return '(%s) under (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, Over):
        return '(%s) over (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, Comma):
        return '(%s), (%s)' % (Show(x.a), Show(x.b))
    if isinstance(x, (int, long)):
        return '%i' % x
    raise NotImplementedError('type %s not handled: %s' % (type(x), x))

Results = collections.namedtuple('Results', ['results'])

_dice_cache = {}
def Calc(x, deadline):
    def DieIfDeadlineExceeded():
        if time.time() > deadline:
            raise OutOfBoundsError("Timeout")
    DieIfDeadlineExceeded()
    if isinstance(x, Expr):
        return Calc(x.e, deadline)
    elif isinstance(x, (int, long)):
        return Results({x: 1.})
    elif isinstance(x, (Min, Max, Sum)):
        results = Calc(x.l, deadline)
        if not isinstance(results.results.keys()[0], tuple):
            return results
        else:
            outcomes = {}
            for r, p in results.results.items():
                DieIfDeadlineExceeded()
                red = Named[type(x)](r)
                if red not in outcomes:
                    outcomes[red] = 0
                outcomes[red] += p
            return Results(outcomes)
    elif isinstance(x, (Add, Sub, Mul, Div)):
        left_result = Calc(x.a, deadline)
        right_result = Calc(x.b, deadline)
        outcomes = {}
        for l, p_l in left_result.results.items():
            if isinstance(l, tuple):
                l = sum(l)
            for r, p_r in right_result.results.items():
                DieIfDeadlineExceeded()
                if isinstance(r, tuple):
                    r = sum(r)
                if isinstance(x, Div) and r == 0:
                    r = float('nan')
                out = Named[type(x)](l, r)
                outcomes.setdefault(out, 0.)
                outcomes[out] += p_l * p_r
        return Results(outcomes)
    elif isinstance(x, Dice):
        if x in _dice_cache:
            return _dice_cache[x]
        if x.n < 1:
            raise OutOfBoundsError("Must roll at least one die")
        if x.n == 1:
            _dice_cache[x] = Results({a: 1./x.d for a in range(1, x.d+1)})
            return _dice_cache[x]
        if x.n > 5:
            raise OutOfBoundsError("Rolling more than 5 dice isn't allowed")
        if x.d > 100:
            raise OutOfBoundsError("Rolling more than 100 sided dice isn't allowed")
        outcomes = {}
        for l in itertools.product(*[range(1, x.d+1)]*x.n):
            DieIfDeadlineExceeded()
            outcomes[l] = x.d**(-x.n)
        _dice_cache[x] = Results(outcomes)
        return _dice_cache[x]
    elif isinstance(x, Comma):
        left_result = Calc(x.a, deadline)
        right_result = Calc(x.b, deadline)
        outcomes = {}
        for l, p_l in left_result.results.items():
            for r, p_r in right_result.results.items():
                res = Named[type(x)](l, r)
                if res not in outcomes:
                    outcomes[res] = 0.
                outcomes[res] += p_l * p_r
        return Results(outcomes)
    elif isinstance(x, (Over, Under)):
        left_result = Calc(x.a, deadline)
        right_result = Calc(x.b, deadline)
        outcomes = {}
        for l, p_l in left_result.results.items():
            for r, p_r in right_result.results.items():
                DieIfDeadlineExceeded()
                if isinstance(r, tuple):
                    r = sum(r)
                if not isinstance(l, tuple):
                    l = [l]
                res = Named[type(x)](l, r)
                if res not in outcomes:
                    outcomes[res] = 0.
                outcomes[res] += p_l * p_r
        return Results(outcomes)
    raise NotImplementedError('type %s not handled: %s' % (type(x), x))

def PercentileIndex(a, ps):
    cdf = a.cumsum()
    cdf /= cdf[-1]
    l = []
    for p in ps:
        l.append(numpy.searchsorted(cdf, p, side='right'))
    return l

def DisplayCalc(res):
    lines = []
    s = sorted(res.results.items(), key=lambda x: x[0])
    has_numbers = True
    if len(s) < 20:
        for a, p in s:
            lines.append('%5r: %.3g' % (a, p))
            has_numbers = has_numbers and isinstance(a, (int, long))
    if has_numbers:
        array = numpy.array(s, dtype=float)
        E = (array[:, 0] * array[:, 1]).sum()
        PERC = [0.01, 0.1, 0.25, 0.5, 0.75, 0.9, 0.99]
        percentile_indices = PercentileIndex(array[:, 1], PERC)
        q = []
        for p, i in zip(PERC, percentile_indices):
            q.append('%.3g %.3g' % (p, array[i, 0]))
        lines.insert(0, 'Quantiles:\n%s\n' % '\n'.join(q))
        lines.insert(0, 'Expected: %.3g' % E)
    return '\n'.join(lines)

def Eval(x, r):
    if isinstance(x, Expr):
        return Eval(x.e, r)
    elif isinstance(x, (int, long)):
        return (x, [])
    elif isinstance(x, (Min, Max, Sum)):
        result, rolls = Eval(x.l, r)
        if isinstance(result, (list, tuple)):
            return (Named[type(x)](result), rolls)
        else:
            return (result, rolls)
    elif isinstance(x, Comma):
        left_result, left_rolls = Eval(x.a, r)
        right_result, right_rolls = Eval(x.b, r)
        return (Named[type(x)](left_result, right_result),
                left_rolls + right_rolls)
    elif isinstance(x, (Over, Under)):
        left_result, left_rolls = Eval(x.a, r)
        right_result, right_rolls = Eval(x.b, r)
        if isinstance(right_result, (tuple, list)):
            right_result = sum(right_result)
        if not isinstance(left_result, (tuple, list)):
            left_result = [left_result]
        return (Named[type(x)](left_result, right_result),
                left_rolls + right_rolls)
    elif isinstance(x, (Add, Sub, Mul, Div)):
        left_result, left_rolls = Eval(x.a, r)
        right_result, right_rolls = Eval(x.b, r)
        if isinstance(left_result, (tuple, list)):
            left_result = sum(left_result)
        if isinstance(right_result, (tuple, list)):
            right_result = sum(right_result)
        if isinstance(x, Div) and right_result == 0:
            right_result = float('nan')
        return (Named[type(x)](left_result, right_result),
                left_rolls + right_rolls)
    elif isinstance(x, Dice):
        if x.n > 1000:
            raise OutOfBoundsError("Rolling more than 1000 dice isn't allowed")
        rolls = [r.randint(1, x.d) for i in xrange(x.n)]
        if x.n <= 5:
            return rolls, map(lambda d: 'd%s: %s' % (x.d, d), rolls)
        else:
            sorted_items = sorted(collections.Counter(rolls).items())
            strings = []
            strings.append("%sd%s:" % (x.n, x.d))
            for val, count in sorted_items:
                strings.append("\t%-4i : %i" % (val, count))
            return rolls, strings
    raise NotImplementedError('type %s not handled: %s' % (type(x), x))
