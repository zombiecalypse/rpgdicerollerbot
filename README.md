# RpgDiceRollerBot #

This is a [Telegram](https://www.telegram.org/) bot (talk to it [here](https://web.telegram.org/#/im?p=@RpgDiceRollerBot)) to roll dice for online RPG.

## What it can do ##

Once deployed to [Google App Engine](https://cloud.google.com/appengine) you can send it the `/roll` command together with a description of what to roll.

### Some Examples: ###

* `/roll d20 + 2`: Roll a 20-sided die and add 2 to the result.
* `/roll 2d6 + 3`: Roll two 6-sided dice, add them, and add 3 to the result.
* `/roll max 2d20 + 2`: Roll two 20-sided dice, pick the better one, and add 2 to the result.
* `/roll min 2d20 + 2`: Roll two 20-sided dice, pick the worse result, and add 2 to the result.
* `/roll d20 + d4 + 2`: Roll a 20-sided die, add a 4-sided die, and add 2 to that.
* `/roll 2 * d12 + 2`: Roll a 12-sided die, double it, and add 2 to the result.
* `/roll 6d10 over 7`: Roll six 10-sided, and count the rolls that are 7 or more.

### Versions ###

* 1.0:
  * Dice grammar `NdM`
  * Arithmetic operation
  * `over` and `under` for pools
  * Limit the pool size to make server not as easy to break
  * Nicer display of large die pools

## How do I get started? ##

Most people will not need to run the code themselves. Just talk to [RpgDiceRollerBot](https://web.telegram.org/#/im?p=@RpgDiceRollerBot). I'm open for pull requests and feature requests on the issue tracker if you want new features.

However I'm not the one keeping you from running it on your own!

1. [Set up a project on Google App Engine](https://cloud.google.com/appengine/docs/python/quickstart)
2. Install the tools to interact with the project, namely `gcloud`
3. Modify `app.yaml` to point to your own App Engine project
4. `gcloud app deploy app.yaml`
5. Go to https://PROJECT-ID.appspot.com/set_webhook?url=https://PROJECT-ID.appspot.com/webhook
6. [Create a Telegram Bot](https://core.telegram.org/bots)
7. Start talking to your Telegram bot

### How to run tests ###

Running the tests requires the [hypothesis library](hypothesis.readthedocs.io). Install it and ideally [pytest](http://doc.pytest.org/en/latest/). Then run `pytest` on the test targets.

## Contributing ##

If you want to implement a new operation

1. Add a new type
2. Add a new parsing rule in dice.py `expr`
3. Add a few examples in the dice_test.py to show that your parsing works and works nicely with the other rules
4. Add a test in dice_test.py that takes subexpressions and creates your operation and `Eval`s it. Then check the return value of the evaluation. This is mostly to check that it won't raise exceptions, and will return something meaningful. This test will currently fail.
5. Add the recursive evaluation.

## Who do I talk to? ##

Contact @zombiecalypse.